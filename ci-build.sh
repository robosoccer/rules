#!/bin/sh

wget -O latexdockercmd.sh -q \
     https://raw.githubusercontent.com/blang/latex-docker/master/latexdockercmd.sh
sed -i 's/:ubuntu/:ctanfull/' latexdockercmd.sh
sh latexdockercmd.sh sh -c 'xelatex main.tex && xelatex main.tex'
